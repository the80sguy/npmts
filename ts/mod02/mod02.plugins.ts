export * from '../npmts.plugins'

import * as gulp from 'gulp'
import * as gulpFunction from 'gulp-function'
import * as gulpSourcemaps from 'gulp-sourcemaps'
import * as gulpTypeScript from 'gulp-typescript'
import * as tapbuffer from 'tapbuffer'

export {
    gulp,
    gulpFunction,
    gulpSourcemaps,
    gulpTypeScript,
    tapbuffer
}
