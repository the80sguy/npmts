# Summary

* [1. Intro](index.md)
* [2. Install](install.md)
* [3. Default Behaviour](default.md)
* [4. Configuration](config.md)
* [5. Examples](examples.md)
* [6. Info](info.md)