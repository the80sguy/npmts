# Info

## Future Scope:
* automatically manage badges in README
* manage tslint to enforce code best practices
* tear down any differences between local and CI environments by using brand new npmdocker

## About the authors:
[![Project Phase](https://mediaserve.lossless.digital/lossless.com/img/createdby_github.svg)](https://lossless.com/)

[![PayPal](https://img.shields.io/badge/Support%20us-PayPal-blue.svg)](https://paypal.me/lossless)

## Legal Info
https://lossless.gmbh