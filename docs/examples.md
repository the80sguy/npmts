# Examples for npmts

## Example Usage in modules:
* [gulp-browser](https://www.npmjs.com/package/gulp-browser)

> We will add more options over time.

## Tips and tricks:

* Use [npmts-g](https://www.npmjs.com/package/npmts-g) to use globally installed npmts and install npmts locally if no global npmts is available.
* Use [npmpage](https://www.npmjs.com/package/npmpage) to create a webpage from coverage reports and TypeDoc for the module
* Use [hosttoday/ht-docker-node:npmts](https://hub.docker.com/r/hosttoday/ht-docker-node/) for speedy CI builds
* Use [npmdocker](https://www.npmjs.com/package/npmdocker) for running tests consistently with docker.