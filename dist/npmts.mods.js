"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const smartsystem_1 = require("smartsystem");
exports.mod00 = new smartsystem_1.LazyModule('./mod00/index', __dirname);
exports.mod01 = new smartsystem_1.LazyModule('./mod01/index', __dirname);
exports.mod02 = new smartsystem_1.LazyModule('./mod02/index', __dirname);
