import { LazyModule } from 'smartsystem';
import * as _mod00 from './mod00/index';
import * as _mod01 from './mod01/index';
import * as _mod02 from './mod02/index';
export declare let mod00: LazyModule<typeof _mod00>;
export declare let mod01: LazyModule<typeof _mod01>;
export declare let mod02: LazyModule<typeof _mod02>;
